/**
 * www.TheAIGames.com 
 * Heads Up Omaha pokerbot
 *
 * Last update: May 07, 2014
 *
 * @author Jim van Eeden, Starapple
 * @version 1.0
 * @License MIT License (http://opensource.org/Licenses/MIT)
 */

/*
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package bot;

import java.util.Random;

import poker.Card;
import poker.HandHoldem;
import poker.PokerMove;

import com.stevebrecher.HandEval;

/**
 * This class is the brains of your bot. Make your calculations here and return the best move with GetMove
 */
public class BotStarter implements Bot {

	/**
	 * Implement this method to return the best move you can. Currently it will return a raise the ordinal value
	 * of one of our cards is higher than 9, a call when one of the cards has a higher ordinal value than 5 and
	 * a check otherwise.
	 * @param state : The current state of your bot, with all the (parsed) information given by the engine
	 * @param timeOut : The time you have to return a move
	 * @return PokerMove : The move you will be doing
	 */
	//First Implementation with Strictly Procedural Decision Making
	/*
	@Override
	public PokerMove getMove(BotState state, Long timeOut) {
		HandHoldem hand = state.getHand();
		String handCategory = getHandCategory(hand, state.getTable())
				.toString();
		System.err.printf("my hand is %s, opponent action is %s, pot: %d\n",
				handCategory, state.getOpponentAction(), state.getPot());

		// Get the ordinal values of the cards in your hand
		int height1 = hand.getCard(0).getHeight().ordinal();
		int height2 = hand.getCard(1).getHeight().ordinal();

		// Return the appropriate move according to our amazing strategy
		// Now checks for empty board (just evaluating our given hand in this
		// case)
		// If the board is empty we check our own hand and act
		// If we have a pair we'll raise
		// If we have a good high card (Jack, Queen, King, Ace) we will call any
		// opponent raises
		// Otherwise we'll check
		if (state.getTable() == null || state.getTable().length == 0) {
			if (height1 == height2) {
				return new PokerMove(state.getMyName(), "raise",
						2000);
			} else if (height1 >= 10 || height2 >= 10) {
				return new PokerMove(state.getMyName(), "call",
						state.getAmountToCall());
			} else {
				return new PokerMove(state.getMyName(), "check", 0);
			}
		}
		// we now have logic for after the first 3 cards are turned
		// first we get the strength of our hand using the stevebrecher package
		// and included method
		// after that we use some logic to affect our logic
		// In the case of the two highest hands (Four of a Kind, and Straight
		// Flush) we will actually just call to extend the hand
		// In the case of Full House and Straight and Flush, we will bump it up
		// a bit to apply pressure
		// Anything above pair will call anything except extreme raises
		// Pair and No Pair will play if no raise occurs, the last two are the
		// only ones that check opposing move
		if (state.getTable().length == 3) {
			HandEval.HandCategory strength = getHandCategory(state.getHand(),
					state.getTable());
			PokerMove OpponentMove = state.getOpponentAction();
			if (strength == HandEval.HandCategory.STRAIGHT_FLUSH
					|| strength == HandEval.HandCategory.FOUR_OF_A_KIND) {
				return new PokerMove(state.getMyName(), "call",
						state.getAmountToCall());
			} else if (strength == HandEval.HandCategory.FULL_HOUSE
					|| strength == HandEval.HandCategory.FLUSH
					|| strength == HandEval.HandCategory.STRAIGHT) {
				return new PokerMove(state.getMyName(), "raise",
						2 * state.getBigBlind());
			} else if (strength == HandEval.HandCategory.THREE_OF_A_KIND
					|| strength == HandEval.HandCategory.TWO_PAIR) {
				if (OpponentMove == null) {
					return new PokerMove(state.getMyName(), "call",
							state.getAmountToCall());
				}
				// otherwise it means opponent has made a move, we check this
				// with amount to call
				else {
					if (state.getAmountToCall() > 3 * state.getBigBlind())
						return new PokerMove(state.getMyName(), "check", 0);
					else
						return new PokerMove(state.getMyName(), "call",
								state.getAmountToCall());
				}
			} else {
				if (OpponentMove == null) {
					return new PokerMove(state.getMyName(), "call",
							state.getAmountToCall());
				}
			}
			if (OpponentMove.getAction() == "raise") {
				return new PokerMove(state.getMyName(), "check", 0);
			}
			return new PokerMove(state.getMyName(), "call",
					state.getAmountToCall());
		}
		// we now have logic for after 4 cards are turned
		// first we get the strength of our hand using the stevebrecher package
		// and included method
		// after that we use some logic to affect our logic
		// In the case of the two highest hands (Four of a Kind, and Straight
		// Flush) we continue to just call to extend the hand
		// In the case of Full House and Straight and Flush, we will check for
		// any sort of
		// raise and then if there is none we will raise ourselves, otherwise
		// just call
		// Anything above pair will call more tentatively, checking the amount
		// Pair and No Pair will play if no raise occurs
		if (state.getTable().length == 4) {
			HandEval.HandCategory strength = getHandCategory(state.getHand(),
					state.getTable());
			PokerMove OpponentMove = state.getOpponentAction();
			if (strength == HandEval.HandCategory.STRAIGHT_FLUSH
					|| strength == HandEval.HandCategory.FOUR_OF_A_KIND) {
				return new PokerMove(state.getMyName(), "call",
						state.getAmountToCall());
			} else if (strength == HandEval.HandCategory.FULL_HOUSE
					|| strength == HandEval.HandCategory.FLUSH
					|| strength == HandEval.HandCategory.STRAIGHT) {
				if (OpponentMove == null) {
					return new PokerMove(state.getMyName(), "raise",
							2 * state.getBigBlind());
				}
				if (OpponentMove.getAction() == "raise") {
					return new PokerMove(state.getMyName(), "call",
							state.getAmountToCall());
				}
				// otherwise
				return new PokerMove(state.getMyName(), "raise",
						2 * state.getBigBlind());
			} else if (strength == HandEval.HandCategory.THREE_OF_A_KIND
					|| strength == HandEval.HandCategory.TWO_PAIR) {
				if (OpponentMove == null) {
					return new PokerMove(state.getMyName(), "call",
							state.getAmountToCall());
				}
				// otherwise it means opponent has made a move, we check this
				// with amount to call
				else {
					if (state.getAmountToCall() > 2 * state.getBigBlind())
						return new PokerMove(state.getMyName(), "check", 0);
					else
						return new PokerMove(state.getMyName(), "call",
								state.getAmountToCall());
				}
			} else {
				if (OpponentMove == null) {
					return new PokerMove(state.getMyName(), "call",
							state.getAmountToCall());
				}
			}
			if (OpponentMove.getAction() == "raise") {
				return new PokerMove(state.getMyName(), "check", 0);
			}
			return new PokerMove(state.getMyName(), "call",
					state.getAmountToCall());
		}
		// we now have logic for after 5 cards are turned
		// first we get the strength of our hand using the stevebrecher package
		// and included method
		// after that we use some logic to affect our logic
		// In the case of the two highest hands (Four of a Kind, and Straight
		// Flush) we now raise slightly to eke out any money but not attract a
		// flop
		// In the case of Full House and Straight and Flush, we will check for
		// any sort of raise, and if it's too big we will just call but
		// otherwise we will raise
		// Anything above pair will call if the amount is not too large, trying
		// to play the hand out
		// Pair and No Pair will play up to a small raise
		if (state.getTable().length == 5) {
			HandEval.HandCategory strength = getHandCategory(state.getHand(),
					state.getTable());
			PokerMove OpponentMove = state.getOpponentAction();
			if (strength == HandEval.HandCategory.STRAIGHT_FLUSH
					|| strength == HandEval.HandCategory.FOUR_OF_A_KIND) {
				return new PokerMove(state.getMyName(), "raise",
						state.getBigBlind() + state.getBigBlind() / 2);
			} else if (strength == HandEval.HandCategory.FULL_HOUSE
					|| strength == HandEval.HandCategory.FLUSH
					|| strength == HandEval.HandCategory.STRAIGHT) {
				if (OpponentMove == null) {
					return new PokerMove(state.getMyName(), "raise",
							2 * state.getBigBlind());
				}
				if (OpponentMove.getAction() == "raise") {
					return new PokerMove(state.getMyName(), "call",
							state.getAmountToCall());
				}
				// otherwise
				return new PokerMove(state.getMyName(), "raise",
						2 * state.getBigBlind());
			} else if (strength == HandEval.HandCategory.THREE_OF_A_KIND
					|| strength == HandEval.HandCategory.TWO_PAIR) {
				if (OpponentMove == null) {
					return new PokerMove(state.getMyName(), "call",
							state.getAmountToCall());
				}
				// otherwise it means opponent has made a move, we check this
				// with amount to call
				else {
					if (state.getAmountToCall() > 5 * state.getBigBlind())
						return new PokerMove(state.getMyName(), "check", 0);
					else
						return new PokerMove(state.getMyName(), "call",
								state.getAmountToCall());
				}
			} else {
				if (OpponentMove == null) {
					return new PokerMove(state.getMyName(), "call",
							state.getAmountToCall());
				}
			}
			return new PokerMove(state.getMyName(), "call",
					state.getAmountToCall());
		}
		return new PokerMove(state.getMyName(), "call", state.getAmountToCall());
	}
	*/
	static long timer;
	static long timeToMove;
	
	@Override
	public PokerMove getMove(BotState state, Long timeOut) {
		HandHoldem hand = state.getHand();
		String handCategory = getHandCategory(hand, state.getTable())
				.toString();
		System.err.printf("my hand is %s, opponent action is %s, pot: %d\n",
				handCategory, state.getOpponentAction(), state.getPot());

		// Get the ordinal values of the cards in your hand
		int height1 = hand.getCard(0).getHeight().ordinal();
		int height2 = hand.getCard(1).getHeight().ordinal();

		// Return the appropriate move according to our amazing strategy
		// Now checks for empty board (just evaluating our given hand in this
		// case)
		// If the board is empty we check our own hand and act
		// If we have a pair we'll raise
		// If we have a good high card (Jack, Queen, King, Ace) we will call any
		// opponent raises
		// Otherwise we'll check
		// No reason to change for probability as there's no hands to calculate
		Random Random = new Random();
		if (state.getTable() == null || state.getTable().length == 0) {
			if (height1 == height2) {
				return new PokerMove(state.getMyName(), "raise",
						100);
			}
			PokerMove OpponentMove = state.getOpponentAction();
			if(OpponentMove == null){
				return new PokerMove(state.getMyName(), "call",
						state.getAmountToCall());
			}
			if(OpponentMove.getAction() == "raise"){
				double randomActionPairs = Random.nextDouble();
				if(randomActionPairs >= .75){
					return new PokerMove(state.getMyName(), "call",
							state.getAmountToCall());
				}
				else{
					return new PokerMove(state.getMyName(), "check", 0); 
				}
			}
		}
		//Now that Probability is programmed in we just start a procedural randomness for the bot
		//where it will play by itself based on the percentages 
		double Prob = getProbOfBetterHand(state.getHand(), state.getTable());
		boolean canFold = canFold(state.getHand(), state.getTable());
		if (state.getTable().length == 3)
			Prob = Prob*.9;
		if(state.getTable().length == 4)
			Prob = Prob*.95;
		PokerMove OpponentMove = state.getOpponentAction();
		double randomAction =  Random.nextDouble();
		if(Prob >= randomAction && canFold == false){
			return new PokerMove(state.getMyName(), "raise",
					50);
		}
		if(Prob >= randomAction && canFold == true){
			double randomAction4 =  Random.nextDouble();
			if(Prob >= randomAction4){
				return new PokerMove(state.getMyName(), "raise",
						50);
			}
			return new PokerMove(state.getMyName(), "call",
						state.getAmountToCall());
		}
		if(canFold != true && Prob < randomAction){
			return new PokerMove(state.getMyName(), "call",
					state.getAmountToCall());
		}
		if(canFold == true && Prob < randomAction){
			double randomAction2 =  Random.nextDouble();
			if(Prob >= randomAction2){
				return new PokerMove(state.getMyName(), "call", state.getAmountToCall());
			}
			if(OpponentMove == null){
				return new PokerMove(state.getMyName(), "call", state.getAmountToCall());
			}
			if (OpponentMove.getAction() == "raise"){
				double randomAction3 =  Random.nextDouble();
				if(randomAction3 >= .4)
					return new PokerMove(state.getMyName(), "check", 0);
				else
					return new PokerMove(state.getMyName(), "call", state.getAmountToCall());
			} 
		}
		return new PokerMove(state.getMyName(), "call", state.getAmountToCall());
	}
	
	/**
	 * Calculates the bot's hand strength, with 0, 3, 4 or 5 cards on the table.
	 * This uses the com.stevebrecher package to get hand strength.
	 * @param hand : cards in hand
	 * @param table : cards on table
	 * @return HandCategory with what the bot has got, given the table and hand
	 */
	public HandEval.HandCategory getHandCategory(HandHoldem hand, Card[] table) {
		if( table == null || table.length == 0 ) { // there are no cards on the table
			return hand.getCard(0).getHeight() == hand.getCard(1).getHeight() // return a pair if our hand cards are the same
					? HandEval.HandCategory.PAIR
					: HandEval.HandCategory.NO_PAIR;
		}
		long handCode = hand.getCard(0).getNumber() + hand.getCard(1).getNumber();
		
		for( Card card : table ) { handCode += card.getNumber(); }
		
		if( table.length == 3 ) { // three cards on the table
			return rankToCategory(HandEval.hand5Eval(handCode));
		}
		if( table.length == 4 ) { // four cards on the table
			return rankToCategory(HandEval.hand6Eval(handCode));
		}
		return rankToCategory(HandEval.hand7Eval(handCode)); // five cards on the table
	}
	
	/**
	 * small method to convert the int 'rank' to a readable enum called HandCategory
	 */
	public HandEval.HandCategory rankToCategory(int rank) {
		return HandEval.HandCategory.values()[rank >> HandEval.VALUE_SHIFT];
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		BotParser parser = new BotParser(new BotStarter());
		parser.run();
	}
	
	//Calculates based all potential cards in their hand the opponent might have, 
	//how many of them would beat the best hand we can make
	//returns the probability of that based on (how many can beat)/(All Possible Hands)
	public double getProbOfBetterHand(HandHoldem hand, Card[] table){
		HandEval.HandCategory strengthOfHand = getHandCategory(hand, table);
		if(strengthOfHand == HandEval.HandCategory.STRAIGHT_FLUSH){
			return 1 - .00032;
		}
		if(strengthOfHand == HandEval.HandCategory.FOUR_OF_A_KIND){
			return 1 - .00279;
		}
		if(strengthOfHand == HandEval.HandCategory.FULL_HOUSE){
			return 1 - .0168;
		}
		if(strengthOfHand == HandEval.HandCategory.FLUSH){
			return 1 - .26;
		}
		if(strengthOfHand == HandEval.HandCategory.STRAIGHT){
			return 1 - .303;
		}
		if(strengthOfHand == HandEval.HandCategory.THREE_OF_A_KIND){
			return 1 - .462;
		}
		if(strengthOfHand == HandEval.HandCategory.TWO_PAIR){
			return 1 - .483;
		}
		if(strengthOfHand == HandEval.HandCategory.PAIR){
			return 1 - .687; 
		}
		if(strengthOfHand == HandEval.HandCategory.NO_PAIR){
			return .2;
		}
		return 1;
		/*
		int handWins = 0;
		int totalHands = 0;
		System.out.println("enter prob of better hand");
		for(int i = 0; i < 52; i++){
			for(int q = 0; q < 52; q++){
				System.out.println(i);
				System.out.println(q);
				Card card1 = new Card(i);
				Card card2 = new Card(q);
				HandHoldem testHand = new HandHoldem(card1, card2);
				if(cardConflict(hand, testHand, table) ==  false){
					HandEval.HandCategory strengthOfTest = getHandCategory(testHand, table);
					if(strengthOfTest.compareTo(strengthOfHand) > 1){
						handWins++;
					}
					totalHands++;
				}
			}
		}
		double Prob = handWins/totalHands;
		return Prob;
		*/
	}
	
	public boolean canFold(HandHoldem hand, Card[] table){
			HandEval.HandCategory strengthOfHand = getHandCategory(hand, table);
			if(strengthOfHand == HandEval.HandCategory.STRAIGHT_FLUSH){
				return false;
			}
			if(strengthOfHand == HandEval.HandCategory.FOUR_OF_A_KIND){
				return false;
			}
			if(strengthOfHand == HandEval.HandCategory.FULL_HOUSE){
				return false;
			}
			if(strengthOfHand == HandEval.HandCategory.FLUSH){
				return false;
			}
			if(strengthOfHand == HandEval.HandCategory.STRAIGHT){
				return false;
			}
			return true;
	}
	//tests to make sure the testHand cards don't conflict with the origHand or table, that there aren't multiples
	public boolean cardConflict(HandHoldem origHand, HandHoldem testHand, Card[] table){
		if(origHand.getCard(0) == testHand.getCard(0)){
			return true;
		}
		if(origHand.getCard(1) == testHand.getCard(0)){
			return true;
		}
		if(origHand.getCard(0) == testHand.getCard(1)){
			return true;
		}
		if(origHand.getCard(1) == testHand.getCard(1)){
			return true;
		}
		if(table.length >= 3){
			if(table[0] == testHand.getCard(0)){
				return true;
			}
			if(table[0] == testHand.getCard(1)){
				return true;
			}
			if(table[1] == testHand.getCard(0)){
				return true;
			}
			if(table[1] == testHand.getCard(1)){
				return true;
			}
			if(table[2] == testHand.getCard(0)){
				return true;
			}
			if(table[2] == testHand.getCard(1)){
				return true;
			}
		}
		if(table.length >= 4){
			if(table[3] == testHand.getCard(0)){
				return true;
			}
			if(table[3] == testHand.getCard(1)){
				return true;
			}
		}
		if(table.length >= 5){
			if(table[4] == testHand.getCard(0)){
				return true;
			}
			if(table[4] == testHand.getCard(1)){
				return true;
			}
		}
		System.out.println("Hand is legal");
		return false;
	}

}
