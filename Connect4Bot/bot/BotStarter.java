// // Copyright 2015 theaigames.com (developers@theaigames.com)

//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at

//        http://www.apache.org/licenses/LICENSE-2.0

//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//	
//    For the full copyright and license information, please view the LICENSE
//    file that was distributed with this source code.

package bot;
import java.util.Random;

/**
 * BotStarter class
 * 
 * Magic happens here. You should edit this file, or more specifically
 * the makeTurn() method to make your bot do more than random moves.
 * 
 * @author Jim van Eeden <jim@starapple.nl>, Joost de Meij <joost@starapple.nl>
 */

public class BotStarter {	
     Field field;

     /**
      * Makes a turn. Edit this method to make your bot smarter.
      *
      * @return The column where the turn was made.
      */
     public int makeTurn() {
    	 /*
    	 if(field.getDisc(0,0) == 0 && field.getDisc(1,0) == 0 && field.getDisc(2,0) == 0 && field.getDisc(3,0) == 0 && field.getDisc(4,0) == 0 && field.getDisc(5,0) == 0 && field.getDisc(6,0) == 0)
    		 return 3;
    	 */
    	 int Move = minMaxStart(1, field, 2); 
         return 1;
     }
    
    public int minMaxStart(int disc, Field inputField, int depth){
    	int bestMove = -1;
    	int bestMoveValue = -100;
    	for(int i = 0; i < 7; i++){
    		int moveValue;
    		Field newState = inputField;
    		if(inputField.isValidMove(i)){
    			inputField.addDisc(i, disc);
    			if(disc == 1)
    				moveValue = minMax(2, inputField, depth--);
    			else
    				moveValue = minMax(1, inputField, depth--);
	    		if(moveValue > bestMoveValue){
	    			bestMoveValue = moveValue;
	    			bestMove = i;
	    		}
    		}
    	return bestMove;
    	}
    	return 0;
    }
     
    public int minMax(int disc, Field inputField, int depth){
    	if(winState(inputField, disc))
			return 100;
		if(loseState(inputField, disc))
			return -100;
    	if(depth == 0){
    		return 0;
    	}
    	int totalWeight = 0;
    	for(int i = 0; i < 7; i++){
    		Field newState = inputField;
    		if(inputField.isValidMove(i)){
    			inputField.addDisc(i, disc);
    			if(disc == 1)
    				totalWeight = totalWeight + minMax(2, inputField, depth--);
    			else
    				totalWeight = totalWeight + minMax(1, inputField, depth--);
    		}
    	}
    	return 0;
    }
    
    public boolean winState(Field inputField, int disc){
    	if(checkColumnWin(inputField, disc))
    		return true;
    	if(checkRowWin(inputField, disc))
    		return true;
    	if(checkDiagWinRight(inputField, disc))
    		return true;
    	if(checkDiagWinLeft(inputField, disc))
    		return true;
    	return false;
    }
    
    public boolean loseState(Field inputField, int disc){
    	if(disc == 1)
    		return winState(inputField, 2);
    	else
    		return winState(inputField, 1);
    }
    
    public boolean checkColumnWin(Field inputField, int disc){
    	for(int Column = 0; Column < inputField.getNrColumns(); Column++){
    		for(int Row = 0; Row < inputField.getNrRows()-3; Row++){
    			if(inputField.getDisc(Column, Row) == disc && inputField.getDisc(Column, Row+1) == disc && inputField.getDisc(Column, Row+2) == disc && inputField.getDisc(Column, Row+3) == disc){
    				return true;
    			}
    		}
    	}
    	return false;
    }
    
	public boolean checkRowWin(Field inputField, int disc){
		for(int Row = 0; Row < inputField.getNrRows(); Row++){
    		for(int Column = 0; Column < inputField.getNrColumns()-3; Column++){
    			if(inputField.getDisc(Column, Row) == disc && inputField.getDisc(Column+1, Row) == disc && inputField.getDisc(Column+2, Row) == disc && inputField.getDisc(Column+3, Row) == disc){
    				return true;
    			}
    		}
    	}
		return false;	
	}
	
	public boolean checkDiagWinRight(Field inputField, int disc){
		for(int Column = 0; Column < inputField.getNrColumns()-3; Column++){
    		for(int Row = 0; Row < inputField.getNrRows()-3; Row++){
    			if(inputField.getDisc(Column, Row) == disc && inputField.getDisc(Column+1, Row+1) == disc && inputField.getDisc(Column+2, Row+2) == disc && inputField.getDisc(Column+3, Row+3) == disc){
    				return true;
    			}
    		}
    	}
    	return false;
	}
	
	public boolean checkDiagWinLeft(Field inputField, int disc){
		for(int Column = 6; Column > 2; Column--){
    		for(int Row = 0; Row < 3; Row++){
    			if(inputField.getDisc(Column, Row) == disc && inputField.getDisc(Column-1, Row+1) == disc && inputField.getDisc(Column-2, Row+2) == disc && inputField.getDisc(Column-3, Row+3) == disc){
    				return true;
    			}
    		}
    	}
		return false;
	}
    
 	public static void main(String[] args) {
 		BotParser parser = new BotParser(new BotStarter());
 		parser.run();
 	}
 	
 }
